<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    $listVideos = ['bg.mp4', 'Saigon.mp4', 'Star.mp4', 'Keyboard.mp4', 'Swimming.mp4'];
    
    $video = $listVideos[rand(0, count($listVideos)-1)];
    
    return view('home', compact('video'));
});

Route::post('/url', 'URLController@url')->name('url');
Route::get('/wu/{slug}', 'URLController@redirect')->name('redirect');

Route::get('/privacy', function () {

    return view('privacy');
});
Route::get('/terms', function () {

    return view('terms');
});
