<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use Crawler;

class URLController extends Controller
{

    public function url(Request $request){
        
        if(!$request->url || !($request->url && filter_var(substr( $request->url, 0, 4 ) === "http" ? $request->url : 'http://'.$request->url, FILTER_VALIDATE_URL))){
            return response()->json(
                json_encode([
                        'status' => false,
                        'url' => ''
                        ])
            );
        }

        $linkSearch = Link::where('url', $request->url)->first();

        if($linkSearch){
            $link = $linkSearch;
        }else{

            $link = new Link();
            $link->url = substr( $request->url, 0, 4 ) === "http" ? $request->url : 'http://'.$request->url;
            $link->title = $request->title;
            $link->description = $request->description;
            $img = substr( $request->img, 0, 4 ) === "http" ? $request->img : 'http://'.$request->img;
            $link->img = $img && filter_var($img, FILTER_VALIDATE_URL) ? $img : '';
            $link->ip = $request->ip();
            $link->slug = ''.substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $link->save();
        }

        return response()->json(
                json_encode([
                    'status' => $link?true:false,
                    'url' => $link ? route('redirect', ['slug' => $link->slug]) : ''
                    ])
        );
    }

    public function redirect(Request $request, $slug){

        $link = Link::where('slug', $slug)->first();

        if(!$link){
            abort(404);
        }
        
        if(Crawler::isCrawler()) {
            return view('tags', compact('link'));
        }
        
        $link->redirects = $link->redirects+1;
        $link->save();

        return redirect()->away($link->url);
    }

}
