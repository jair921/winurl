<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $langArr = array("en", "es");
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $languages = explode('-', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        } else {
            $languages[0] = "es";
        }

        if (in_array($languages[0], $langArr)){
            \App::setLocale($languages[0]);
        }else{
            \App::setLocale('es');
        }

        return $next($request);
    }
}
