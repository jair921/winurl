<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 11:23:39 -0500.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Link
 * 
 * @property int $id
 * @property string $url
 * @property string $slug
 * @property string $ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Link extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'link';

	protected $fillable = [
		'url',
		'slug',
		'ip',
		'title',
		'description',
		'img',
                'redirects'
	];
}
