<?php

return [
 'description' => 'Acortador de enlaces gratuito y sin registros',
 'titlePage' => 'Acortador de enlaces',
 'pageDesc' => 'Acortador de enlaces, <strong>gratuito</strong> y <strong>sin registros</strong> !!',
 'placeholderURL' => 'ingrese la URL...',
 'placeholderTitle' => 'ingrese el título (opcional)...',
 'placeholderDescription' => 'ingrese la descripción (opcional)...',
 'placeholderImg' => 'ingrese la ruta de la imagen (opcional)...',
 'cut' => 'Acortar',
 'link' => 'Enlace',
 'yourLink' => 'Su enlace es: ',
 'linkError' => 'Intente de nuevo, por favor.',
 'defaultTitle' => 'winURL - click para visitar',
];
