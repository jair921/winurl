<?php

return [
  'description' => 'Free link shortener and without sign up',
  'titlePage' => 'Link shortener',
  'pageDesc' => 'Link shortener, <strong> free </ strong> and <strong> without sign up </ strong> !!',
  'placeholderURL' => 'enter the URL ...',
  'placeholderTitle' => 'enter the title (optional) ...',
  'placeholderDescription' => 'enter the description (optional) ...',
  'placeholderImg' => 'enter the path of the image (optional) ...',
  'cut' => 'Shorten',
  'link' => 'Link',
  'yourLink' => 'Your link is:',
  'linkError' => 'Try again, please.',
  'defaultTitle' => 'winURL - click for visit',
];
