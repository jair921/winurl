<html>
    <head>
    <title>Condiciones del servicio </title>
    </head>
    <body>
    
        Condiciones del servicio <br>

        
        Las presentes Condiciones Generales de Uso, Política de Privacidad y Venta (en adelante, las "Condiciones Generales") regulan el uso del sitio web winurl (en adelante, el Sitio Web). (en adelante "winurl"), pone a disposición de las personas que accedan a su Sitio Web con el fin de proporcionales información sobre productos y servicios, propios y/o de terceros colaboradores, y facilitarles el acceso a los mismos, así como la contratación de servicios y bienes por medio de la misma (todo ello denominado conjuntamente los "Servicios").

Para contactar con winurl, puede utilizar la dirección de correo postal arriba indicada, así como la dirección de correo electrónica broker.jamm@gmail.com
Por la propia naturaleza del Sitio Web, así como de su contenido y finalidad, la práctica totalidad de la navegación que se puede llevar a cabo por el mismo ha de hacerse gozando de la condición de Cliente, la cual se adquiere según los procedimientos recogidos en la misma. Por lo tanto, la citada condición de Cliente supone la adhesión a las Condiciones Generales en la versión publicada en el momento en que se acceda al Sitio Web. winurl se reserva el derecho de modificar, en cualquier momento, la presentación y configuración del Sitio Web, así como las presentes Condiciones Generales. Por ello, winurl recomienda al Cliente leer el mismo atentamente cada vez que acceda al Sitio Web.

En cualquier caso, existen páginas del Sitio Web accesibles al público en general, respecto a las cuales winurl también desea cumplir con sus obligaciones legales, así como regular el uso de las mismas. En este sentido, los usuarios que accedan a estas partes del Sitio Web aceptan quedar sometidos, por el hecho de acceder a las citadas páginas, por los términos y condiciones recogidos en estas Condiciones Generales, en la medida que ello les pueda ser de aplicación.

Por último, por la naturaleza propia del presente Sitio Web, es posible que se modifiquen o incluyan cambios en el contenido de las presentes Condiciones Generales. Por esto, el Cliente, así como otros usuarios que no gocen de esta condición, quedan obligados a acceder a las presente Condiciones Generales cada vez que accedan al Sitio Web, asumiendo que les serán de aplicación las condiciones correspondientes que se encuentren vigentes en el momento de su acceso.
    </body>
</html>