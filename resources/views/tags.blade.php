<html>
    <head>
        <title>{{ $link->title ? $link->title : trans('site.defaultTitle') }}</title>

        <meta name="twitter:title" value="{{ $link->title ? $link->title : trans('site.defaultTitle') }}">

        <meta property="og:title" content="{{ $link->title ? $link->title : trans('site.defaultTitle') }}"/>
        <meta property="og:type" content="website" />
        <meta property="og:url" content="{{ route('redirect', ['slug' => $link->slug])}}" />
        <meta property="fb:app_id" content="330690324335831" />
        
        <meta property="twitter:site" content="{{ route('redirect', ['slug' => $link->slug])}}" />
        @if($link->description)
            <meta property="og:description" content="{{ $link->description }}"/>
            <meta name="twitter:description" content="{{ $link->description }}" />
        @endif
        @if($link->img)
            <meta property="og:image" content="{{ $link->img }}"/>
            <meta property="og:image:width" content="200"/>
            <meta property="og:image:height" content="200"/>
            <meta name="twitter:image" content="{{ $link->img }}">
        @endif
    </head>
    <body>
        
    </body>
</html>