<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="{{ trans('site.description') }}">
        <meta name="author" content="Jair Martinez">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <title>{{ config('app.name') }}</title>
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('css/coming-soon.min.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="overlay"></div>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="{{ asset('mp4/'.$video) }}" type="video/mp4">
        </video>
        <div class="masthead">
            <div class="masthead-bg"></div>
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 my-auto">
                        <div class="masthead-content text-white py-5 py-md-0">
                            <h1 class="mb-3">{{ trans('site.titlePage') }}</h1>
                            <p class="mb-5">{!! trans('site.pageDesc') !!}</p>
                            <div class="input-group input-group-newsletter">
                                <input type="url" class="form-control" placeholder="{{ trans('site.placeholderURL') }}" aria-label="{{ trans('site.placeholderURL') }}" id="urlCut" required="">
                            </div>
                            <div class="input-group input-group-newsletter">
                                <input type="text" class="form-control" placeholder="{{ trans('site.placeholderTitle') }}" aria-label="{{ trans('site.placeholderTitle') }}" id="urlTitle" maxlength="120">
                            </div>
                            <div class="input-group input-group-newsletter">
                                <input type="text" class="form-control" placeholder="{{ trans('site.placeholderDescription') }}" aria-label="{{ trans('site.placeholderDescription') }}" id="urlDescription" maxlength="200">
                            </div>
                            <div class="input-group input-group-newsletter">
                                <input type="url" class="form-control" placeholder="{{ trans('site.placeholderImg') }}" aria-label="{{ trans('site.placeholderImg') }}" id="urlImage" maxlength="700">
                            </div>
                            <div style="text-align: right; margin-top: 10px;">
                                <button class="btn btn-secondary" type="button" id="cutBtn">{{ trans('site.cut') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-icons">
            <ul class="list-unstyled text-center mb-0">
                <li class="list-unstyled-item">
                    <a href="#">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="list-unstyled-item">
                    <a href="#">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li class="list-unstyled-item">
                    <a href="#">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>

        <p id="url" data-url="{{ route('url') }}"></p>
        <div id="urlModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('site.link') }}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                    <div id="ok">
                        <div class="row">
                            <div class="col-md-3">
                                <p>{{ trans('site.yourLink') }}</p>
                            </div>
                            <div class="col-md-8">
                                <div class="input-group mb-3 col-md-12">
                                    <input type="text" class="form-control" readonly="" id="urlShort">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i id="copyCB" class="fa fa-clipboard" style="cursor: pointer;"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="error">
                        <p>{{ trans('site.linkError') }}</p>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Bootstrap core JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Custom scripts for this template -->
        <script src="{{ asset('js/coming-soon.min.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124367015-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124367015-1');
</script>

    </body>
</html>
